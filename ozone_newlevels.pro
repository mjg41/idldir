; IDL Version 7.1.1 (linux x86 m32)
; Journal File for Andrew Bushell
; Working directory: ~/idldir
; Date: Fri Aug 12 21:26:19 2011

; -------------------------------- 
; If in tIDL, return to native IDL
; -------------------------------- 
waveoff 
; ------------------------------------------------------------------------------
; Read in netCDF raw data, generate interpolated zonal mean field, output netCDF
; ------------------------------------------------------------------------------
; Need to expand $DATADIR explicitly as /data/... or NetCDF complains?
;datadir='/data/users/mgriffit'
datadir='/data/users/mgriffit'
; Next specify the input netCDF datafile name
filename='/data/nwp1/magmp/ozone/CMIP5/raw/new_SPARC_O3_rebuild_1990-2010.L60.ncdf'
; Then the location and start of output netCDF datafile name for Xancil
filestem=datadir+'/ozone/CMIP5/Ozone_CMIP5_SPARCex_'
; Optional specification [,dirnamextras=dirnamex] of accessory files location
;dirnamex=datadir+'/ozone/CMIP5'
datanote='S.Osprey rebuild of SPARC(II)'
years=1994+indgen(12)
; ------------------------
; Adjust grids as required
; ------------------------
; Number of levels 
; * requires a levels file in ,dirnamextras=''
; * e.g. datadir+'/ozone/CMIP5/O3_z_theta_L85.txt'
nvertres=88
dirnamextras='/net/home/h05/magjk/vertlevs/'
; Horizontal grid 
; * first is the "N" number (i.e. half of points round latitude)
; * second is number of latitudes (phi) points (if this is not entered, program
;   will attempt to default from a list of recognised Nxx resolutions) 
; nhorizres=[96,145] - is this still true with /endgame?
nhorizres=[96]
endgame=1
; For Ozone Masterfiles, want points on poles to interpolate, hence chose 
; nhorizres=[24]
; endgame=0
;
; LAM 3D masterfile also has
; lamlong=360
;
; Optional label to separate (for instance) L70 flavours 
; levlabel='UK40'
; --------------------------------------------------------------------------
; Output file from sparcoz_interpol is ready for Xancil to transform to .anc
; Note that output for 360 or Gregorian calendars is identical (only difference 
; is that Xancil toggles a "360-calendar?" Bit between True and False)
; --------------------------------------------------------------------------
sparcoz_interpol,filename,filestem,nvertres,nhorizres,/massmix,/clim,/quiet,year=years,datanote=datanote,dirnamextras=dirnamextras,endgame=endgame
;; File contains the following variables: 
;   0 longitude
;   1 latitude
;   2 pressure
;   3 t
;   4 O3
;; 
;; Variable Dimensions: 
;   0 longitude, LEN =           72
;   1 latitude, LEN =           37
;   2 pressure, LEN =           31
;   3 t, LEN =          240(Unlimited dim) 
;; 
;; Variables: 
;longitude (FLOAT ) Dimension Ids = [ 0 ]
;;   Attribute: units= degrees_east
;;   Attribute: point_spacing= even
;;   Attribute: modulo=  
;latitude (FLOAT ) Dimension Ids = [ 1 ]
;;   Attribute: units= degrees_north
;;   Attribute: point_spacing= even
;pressure (FLOAT ) Dimension Ids = [ 2 ]
;;   Attribute: units= level
;t (FLOAT ) Dimension Ids = [ 3 ]
;;   Attribute: units= days since 1850-01-01 00:00:00
;;   Attribute: calendar= 360_day
;;   Attribute: time_origin= 01-JAN-1850:00:00:00
;O3 (FLOAT ) Dimension Ids = [ 0 1 2 3 ]
;;   Attribute: source=  
;;   Attribute: name= O3
;;   Attribute: title= O3OZONE
;;   Attribute: date= 01/01/50
;;   Attribute: time= 00:00
;;   Attribute: long_name= O3OZONE
;;   Attribute: units=  
;;   Attribute: missing_value=   2.00000e+20
;;   Attribute: _FillValue=   2.00000e+20
;;   Attribute: valid_min=   4.86968e-09
;;   Attribute: valid_max=   2.00000e+20
;; Taking zonal mean ...
;; Processing near-surface NaNs ...
;; Interpolating in the horizontal ...
;; Reading in the SPARC Temperature climatology $DATADIR/ozone/CMIP5/sparc-T-climo.ascii
;; Reading in the model height levels from $DATADIR/ozone/CMIP5/O3_z_theta_L85.txt
;; Interpolating in the vertical ... 
;; Creating mean monthly climatology ... 
;; Processing of data successfully completed.
;; Prepare output to file $DATADIR/ozone/CMIP5/Ozone_CMIP5_SPARCex_M1994-2005_N96L88_li-ch.nc
; Your file has been written. Mischief managed!
; % Program caused arithmetic error: Floating divide by 0
; % Program caused arithmetic error: Floating underflow
; % Program caused arithmetic error: Floating illegal operand
;
; -------------------------------------------
; For file just constructed, inspect contents
; -------------------------------------------
; Note that this filename currently hardwired to N96L88
filename2=datadir+'/ozone/CMIP5/Ozone_CMIP5_SPARCex_M1994-2005_N96L88_li-ch.nc'
sparcoz_grab,filename2,/verbose,$
lono=long2,lato=lat2,levo=zlev2,timeo=time2,intervals=month2,ozoneo=ozone2,nameso=name2
;; File contains the following variables: 
;   0 longitude
;   1 latitude
;   2 hybrid_ht
;   3 t
;   4 O3
;; 
;; Variable Dimensions: 
;   0 longitude, LEN =            1
;   1 latitude, LEN =          145
;   2 hybrid_ht, LEN =           85
;   3 t, LEN =           12(Unlimited dim) 
;; 
;; Variables: 
;longitude (FLOAT ) Dimension Ids = [ 0 ]
;;   Attribute: units= degrees_east
;;   Attribute: point_spacing= even
;latitude (FLOAT ) Dimension Ids = [ 1 ]
;;   Attribute: units= degrees_north
;;   Attribute: point_spacing= even
;hybrid_ht (FLOAT ) Dimension Ids = [ 2 ]
;;   Attribute: units= m
;;   Attribute: positive= up
;t (FLOAT ) Dimension Ids = [ 3 ]
;;   Attribute: units= days since 0000-01-01 00:00:00
;;   Attribute: calendar= 360_day
;;   Attribute: time_origin= 01-JAN-0000:00:00:00
;O3 (FLOAT ) Dimension Ids = [ 0 1 2 3 ]
;;   Attribute: source= Climatology from S.Osprey rebuild of SPARC(II)::
;;   Attribute: name= O3
;;   Attribute: title= Ozone mass mixing ratio
;;   Attribute: long_name= O3, SPARC Ozone
;;   Attribute: units= kg kg^-1
;
oz2dims=size(ozone2,/dimens)
print,oz2dims
; print,size(ozone2)
;           4           1         145          85          12           4      147900
checkarray,ozone2,/outer,/crynan,/neg
;; i: 0  ; NaNs:            0; Min:   1.96427e-08; Max:   1.62001e-05
;; i: 1  ; NaNs:            0; Min:   1.94847e-08; Max:   1.65407e-05
;; i: 2  ; NaNs:            0; Min:   1.88414e-08; Max:   1.65379e-05
;; i: 3  ; NaNs:            0; Min:   2.05723e-08; Max:   1.61200e-05
;; i: 4  ; NaNs:            0; Min:   2.03664e-08; Max:   1.56120e-05
;; i: 5  ; NaNs:            0; Min:   2.02725e-08; Max:   1.53838e-05
;; i: 6  ; NaNs:            0; Min:   2.30593e-08; Max:   1.54854e-05
;; i: 7  ; NaNs:            0; Min:   2.24159e-08; Max:   1.59321e-05
;; i: 8  ; NaNs:            0; Min:   2.15604e-08; Max:   1.63563e-05
;; i: 9  ; NaNs:            0; Min:   2.04253e-08; Max:   1.62719e-05
;; i: 10  ; NaNs:            0; Min:   1.78654e-08; Max:   1.58430e-05
;; i: 11  ; NaNs:            0; Min:   2.01206e-08; Max:   1.58011e-05
;
; UM value of Ratio Molar Mass of Water to Dry Air
epsiloz=48./28.
ppmv=1.0E+06/epsiloz
unimv=' ppm (vol) '
coz=[0.,(0.01*[findgen(10)+1.]),(0.2*[findgen(50)+1.])]
loz1=findgen(11)
cloz1=replicate(1,size(loz1,/N_elements))
lozd=[0.1,0.2,0.4]
clozd=replicate(1,size(lozd,/N_elements))
minoz=0.0
zrange=[0.,90.0]
tozone='N'+strtrim(string(nhorizres[0]),2)+'-L'+strtrim(string(oz2dims[2]),2)+' Climatological Ozone (ppmv)'
ozoneljan=ppmv*reform(ozone2[0,*,*,0])
ozonelapr=ppmv*reform(ozone2[0,*,*,3])
ozoneljul=ppmv*reform(ozone2[0,*,*,6])
ozoneloct=ppmv*reform(ozone2[0,*,*,9])
;
mydevice= !D.NAME
set_plot, 'PS'
!p.multi=[0,2,2]
!P.font = 0 
device, /helvetica, /bold  
device, /landscape
device,/color
; loadct,39
loadct,40
;
contour,ozoneljan,lat2,0.001*zlev2,/cell_fill,levels=coz,min_value=minoz,Ymargin=[6,4],$
ytitle='Height / km', ycharsize = 1.1, yrange=zrange,/ystyle,yticks=9,$
xtitle='Latitude / Degrees', xcharsize = 1.1, xrange=[-90.,90.],/xstyle,xticks=6,$
title='Jan '+tozone,charsize=1.,background=255
contour,ozoneljan,lat2,0.001*zlev2,/overplot,levels=loz1,/noerase,min_value=minoz,$
c_labels=cloz1
contour,ozoneljan,lat2,0.001*zlev2,/overplot,levels=lozd,/noerase,min_value=minoz,$
c_labels=clozd,c_linesty=5
;
contour,ozonelapr,lat2,0.001*zlev2,/cell_fill,levels=coz,min_value=minoz,Ymargin=[6,4],$
ytitle='Height / km', ycharsize = 1.1, yrange=zrange,/ystyle,yticks=9,$
xtitle='Latitude / Degrees', xcharsize = 1.1, xrange=[-90.,90.],/xstyle,xticks=6,$
title='Apr '+tozone,charsize=1.,background=255
contour,ozonelapr,lat2,0.001*zlev2,/overplot,levels=loz1,/noerase,min_value=minoz,$
c_labels=cloz1
contour,ozonelapr,lat2,0.001*zlev2,/overplot,levels=lozd,/noerase,min_value=minoz,$
c_labels=clozd,c_linesty=5
;
contour,ozoneljul,lat2,0.001*zlev2,/cell_fill,levels=coz,min_value=minoz,Ymargin=[6,4],$
ytitle='Height / km', ycharsize = 1.1, yrange=zrange,/ystyle,yticks=9,$
xtitle='Latitude / Degrees', xcharsize = 1.1, xrange=[-90.,90.],/xstyle,xticks=6,$
title='Jul '+tozone,charsize=1.,background=255
contour,ozoneljul,lat2,0.001*zlev2,/overplot,levels=loz1,/noerase,min_value=minoz,$
c_labels=cloz1
contour,ozoneljul,lat2,0.001*zlev2,/overplot,levels=lozd,/noerase,min_value=minoz,$
c_labels=clozd,c_linesty=5
;
contour,ozoneloct,lat2,0.001*zlev2,/cell_fill,levels=coz,min_value=minoz,Ymargin=[6,4],$
ytitle='Height / km', ycharsize = 1.1, yrange=zrange,/ystyle,yticks=9,$
xtitle='Latitude / Degrees', xcharsize = 1.1, xrange=[-90.,90.],/xstyle,xticks=6,$
title='Oct '+tozone,charsize=1.,background=255
contour,ozoneloct,lat2,0.001*zlev2,/overplot,levels=loz1,/noerase,min_value=minoz,$
c_labels=cloz1
contour,ozoneloct,lat2,0.001*zlev2,/overplot,levels=lozd,/noerase,min_value=minoz,$
c_labels=clozd,c_linesty=5
;
device, /close_file
set_plot, mydevice
;
; -----------------------------------------------
; Run comparisons of two different input datasets
; -----------------------------------------------
;filename=datadir+'/ozone/CMIP5/raw/new_SPARC_O3_rebuild_1990-2010.L60.ncdf'
filename='/data/nwp1/magmp/ozone/CMIP5/raw/new_SPARC_O3_rebuild_1990-2010.L60.ncdf'

sparcoz_grab,filename,/verbose,year=[2005],/mdi2nan,$
lono=longs,lato=lats,levo=plevs,timeo=times,intervalso=month05,ozoneo=ozone05,nameso=names
nlat05=size(lats,/N_elements)
nlon05=size(longs,/N_elements)
npress05=size(plevs,/N_elements)
;
checkarray,ozone05,/outer,/neg
;
;filenamec=datadir+'/ozone/CMIP5/raw/Ozone_CMIP5_ACC_SPARC_2000-2009_T3M_O3.nc'
filenamec='/data/nwp1/magmp/ozone/CMIP5/raw/Ozone_CMIP5_ACC_SPARC_2000-2009_T3M_O3.nc'
sparcoz_grab,filenamec,/verbose,year=[2005],$
lono=longc,lato=latc,levo=plevc,timeo=timec,intervalso=month05c,ozoneo=ozone05c,nameso=namec
nlat05c=size(latc,/N_elements)
nlon05c=size(longc,/N_elements)
npress05c=size(plevc,/N_elements)
;
; UM value of Ratio Molar Mass of Water to Dry Air
epsiloz=48./28.
ppmv=1.0E+06/epsiloz
unimv=' ppm (vol) '
coz=[0.,(0.01*[findgen(10)+1.]),(0.2*[findgen(50)+1.])]
loz1=findgen(11)
cloz1=replicate(1,size(loz1,/N_elements))
lozd=[0.1,0.2,0.4]
clozd=replicate(1,size(lozd,/N_elements))
minoz=0.0
prange=[1000.,0.01]
tozone='2005 Ozone'
dcoz=[([findgen(20)-20.]),(0.2*[findgen(21)])]
ndcoz=dcoz[2*indgen(10)]
clozn=replicate(1,size(ndcoz,/N_elements))
pdcoz=dcoz[5*(indgen(5)+4)]
clozp=replicate(1,size(pdcoz,/N_elements))
unidmv=' ppb (vol) '
;
ozmar05=reform(ozone05[*,*,*,2],nlon05,nlat05*npress05)
ozmartmp=fltarr(nlat05*npress05)
for ii=0,(nlat05*npress05)-1 do ozmartmp[ii]=mean(ozmar05[*,ii],/nan)
zozmar05=ppmv*reform(ozmartmp,nlat05,npress05)
ozmar05c=reform(ozone05c[*,*,*,2],nlon05c,nlat05c*npress05c)
ozmartmp=fltarr(nlat05c*npress05c)
for ii=0,(nlat05c*npress05c)-1 do ozmartmp[ii]=mean(ozmar05c[*,ii],/nan)
zozmar05c=ppmv*reform(ozmartmp,nlat05c,npress05c)
dzozmar05=zozmar05
dzozmar05[*,0:npress05c-1]=1000.*(zozmar05[*,0:npress05c-1]-zozmar05c[*,*])
dzozmar05[*,npress05c:npress05-1]=0.0
;
print,max(dzozmar05[where(finite(dzozmar05) gt 0)])
;
mydevice= !D.NAME
set_plot, 'PS'
!P.font = 0 
device, /helvetica, /bold  
device, /landscape
device,/color
!p.multi=[0,3,1]
loadct,40
;
contour,zozmar05,lats,plevs,/cell_fill,levels=coz,min_value=minoz,Ymargin=[6,4],$
ytitle='Pressure / hPa', ycharsize = 1.1, yrange=prange,/ystyle,/ylog,$
xtitle='Latitude / Degrees', xcharsize = 1.1, xrange=[-90.,90.],/xstyle,xticks=6,$
title='Mar NCAS '+tozone+unimv,charsize=1.,background=255
contour,zozmar05,lats,plevs,/overplot,levels=loz1,/noerase,min_value=minoz,$
c_labels=cloz1
contour,zozmar05,lats,plevs,/overplot,levels=lozd,/noerase,min_value=minoz,$
c_labels=clozd,c_linesty=5
;
contour,zozmar05c,latc,plevc,/cell_fill,levels=coz,min_value=minoz,Ymargin=[6,4],$
ytitle='Pressure / hPa', ycharsize = 1.1, yrange=prange,/ystyle,/ylog,$
xtitle='Latitude / Degrees', xcharsize = 1.1, xrange=[-90.,90.],/xstyle,xticks=6,$
title='Mar CMIP5 '+tozone+unimv,charsize=1.,background=255
contour,zozmar05c,latc,plevc,/overplot,levels=loz1,/noerase,min_value=minoz,$
c_labels=cloz1
contour,zozmar05c,latc,plevc,/overplot,levels=lozd,/noerase,min_value=minoz,$
c_labels=clozd,c_linesty=5
;
loadct,17
contour,dzozmar05,lats,plevs,/cell_fill,levels=dcoz,Ymargin=[6,4],$
ytitle='Pressure / hPa', ycharsize = 1.1, yrange=prange,/ystyle,/ylog,$
xtitle='Latitude / Degrees', xcharsize = 1.1, xrange=[-90.,90.],/xstyle,xticks=6,$
title='Mar New - CMIP '+tozone+unidmv,charsize=1.,background=255
contour,dzozmar05,lats,plevs,/overplot,levels=pdcoz,/noerase,$
c_labels=clozp
contour,dzozmar05,lats,plevs,/overplot,levels=ndcoz,/noerase,$
c_labels=clozn,c_linesty=5
;
device, /close_file
set_plot, mydevice
;
end
