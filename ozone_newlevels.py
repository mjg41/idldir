### Script to create new ozone ancillary from level set ###

import numpy as np
from sparcoz_interpol import sparcoz_interpol

# Set root directory

rootdir = '/Users/mjg41/Documents/PhD/Ancillary_Creation/'

# Set input directory and input files

indir = rootdir + 'ozone/CMIP5/raw/'
input_ozone = indir + 'new_SPARC_O3_rebuild_1990-2010.L60.ncdf'

# Set output

outdir = rootdir + 'ozone/CMIP5/'

# Code origin

datanote = 'S.Osprey rebuild of SPARC(II)'

# Years of data

years = 1994 + np.arange(12)

## Specify grid ##

# Vertical levels - requires levels file in indir

n_vert_res = 100

# Horizontal levels
# * first is the "N" number (i.e. half of points round latitude)
# * second is number of latitudes (phi) points (if this is not entered, program
# * will attempt to default from a list of recognised Nxx resolutions)
# endgame option has 1 fewer latitude points - default is true in sparcoz_interpol

n_horiz_res = [96]

# Create the netcdf

sparcoz_interpol(input_ozone, outdir, n_vert_res, n_horiz_res, verbose=True, \
				 years=years, levlabel = '120km')


# IN GENERAL NEED TO CHANGE n_vert_res AND levlabel
