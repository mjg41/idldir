; IDL Version 7.1.1 (linux x86 m32)
;+
; NAME: sparcoz_interpol
; PURPOSE: Process SPARC(II) Ozone data from NetCDF file and write to NetCDF.
; CATEGORY: Data Processing
; CALLING SEQUENCE: 
; sparcoz_interpol,'NetCDF_filename','NetCDF_filestem',nmodlev,nmodgrid,[...]
; INPUTS: filename= full /directory/name of netCDF file from the Ozone dataset
;       : filestem= '/directory/filename_start' (year/grid descriptors appended)
;       : inlev= integer, number of model levels onto which to interpolate
;       : ingrid= integer, specifies [half-zonal (96==N96), meridional] resolution 
;                 components of horizontal model grid onto which to interpolate
;                 data. 
;                 Note that if routine recognizes grid it is not necessary to 
;                 specify the meridional resolution component.
; OPTIONAL INPUT PARAMETERS: year= array of years to subselect (default is all)
;                          : datanote= string, note on source of the data
;                          : dirnamextras= directory containing extra inputfiles
;                          : levlabel= string to differentiate equal numlevels
;                          : multiply= multiply the ozone by a set amount
;                          : lamlong= number of longitudes for LAM 3D smearing
; OPTIONAL KEYWORD PARAMETERS : quiet - stop printout from ncdf_probe, etc.
;                             : massmix - sets data to be in mass mixing ratio
;                             :     (used when creating an ancillary from data)
;                             : clim - sets creation of mean climatology from
;                             :        timeseries
;                             : endgame - map onto grid offest from poles
; OPTIONAL OUTPUT PARAMETERS: 
; SIDE EFFECTS: None observed
; RESTRICTIONS: Format assumptions (e.g. that unprocessed data is on pressure) 
; PROCEDURE: sparcoz_grab, checkarray
; MODIFICATION HISTORY: Created by Andrew Bushell, Thu Jul 28 16:54:32 2011
;                       Follows steps in interpol_o3_cmip5_v1.09.pro
;            [ Authors: Mauro Dall'Amico, Dep of Meteorology, Reading, 2007-Feb]
; 	     [ Scott Osprey, Chris Bell, updated for new SPARC ozone (28-08-09)]
;                     : Modified by Andrew Bushell, 12 Aug 2011 (tidy code)
;                     : Modified by Dan Copsey, 16 Mar 2012 (support ENDGame)
;                     : Modified by Andrew Bushell, 08 Jul 2015 (LAM '3D' option)
;-
;
pro sparcoz_interpol,filename,filestem,inlev,ingrid,quiet=quiet,massmix=massmix,$
clim=clim,endgame=endgame,year=year,datanote=datanote,dirnamextras=dirnamextras,$
levlabel=levlabel,multiply=multiply,lamlong=lamlong

; Set Defaults
if (size(multiply,/N_elements) eq 0) then multiply=1.0
if (size(datanote,/N_elements) eq 0) then $ 
  data_from='S.Osprey rebuild of SPARC(II)'  else  data_from=datanote
if (size(dirnamextras,/N_elements) eq 0) then $ 
  dirnamesuppl='/data/nwp1/magmp/ozone/CMIP5'  else  dirnamesuppl=dirnamextras
verbose=(keyword_set(quiet) eq 0)
;
; ----------------------------------------------------------------------
; Specifiy desired horizonal, vertical resolutions of interpolated data:
; ----------------------------------------------------------------------
if (size(ingrid,/N_elements) lt 2) then  begin & $ 
  CASE ingrid[0] OF  & $
    24: puntisuy=37   & $
  
    48: puntisuy=73   & $
  
    96: puntisuy=145   & $
    
    216: puntisuy=325   & $
    
    320: puntisuy=481   & $
    
    512: puntisuy=769   & $
    
    768: puntisuy=1153   & $
    
    ELSE: STOP, '; Error, unexpected horizontal resolution!'  & $
  EndCASE  & $
  if keyword_set(endgame) then puntisuy=puntisuy-1 & $
endif else puntisuy=ingrid[1]
puntisux=2*ingrid[0]
;
livelliv=inlev
IF ((inlev eq 38)+(inlev eq 60)+(inlev eq 70)+(inlev eq 85)+(inlev eq 88)+(inlev eq 120) EQ 0) THEN $
  print, '; Warning unexpected vertical resolution: check you have a levels file!'
;
; ---------------------------------------------------------------
; Title indication of linear interpolation and O3 above model lid
; having been chopped off:
; ---------------------------------------------------------------
if (size(levlabel,/N_elements) gt 0) then colo3=levlabel+'_li-ch' $
  else colo3='_li-ch' 
sudno='N'+strtrim(string(ingrid[0]),2)
nolev='L'+strtrim(string(inlev),2)
; Note in the source attribute of the final netcdf file:
IF (keyword_set(clim)) THEN note='Climatology from ' ELSE note='Time series, '
data_from=note+data_from+':'
;
; The following costants are not to be changed throughout the program:
Navo=6.022*10^(4) ; (10^22 molecules kMol^-1)
grav=9.80665      ; (m s^-2) same value as for the UM 6.1
Ride=8314.        ; (J kg^-1 K^-1)
Mair=28.966       ; (kg kMol^-1) as in the UM 6.1, see M_AIR
sealevpr=1013.25  ; (hPa) and is assumed constant at all latitudes
Mozo=48.          ; (kg kMol^-1)
; ------------------------------------------------
; Read in a selected sub-period of input data file
; and replace MDI by NaNs
; ------------------------------------------------
sparcoz_grab,filename,verbose=verbose,/mdi2nan,mdio=oz1mdi,year=year,$
lono=lon1,lato=lat1,levo=press1,timeo=timefull,intervals=time1,ozoneo=ozono1
;
nt1=size(time1,/N_elements)
timint=time1[1]-time1[0]
daycount=(timint gt 1.)
; Allow for sudden switch to counting time in (360-calendar) days, not months!
; Note that zeit is counted in years, whatever the input data.
if (keyword_set(daycount))  then $
  zeit=1850.+[time1+15.0]/360.  else  $
  zeit=1850.+[time1+0.5]/12.
;-----
nlat1=size(lat1,/N_elements)
nlon1=size(lon1,/N_elements)*1L
npress1=size(press1,/N_elements)
ntime=size(zeit,/N_elements)
;
; -------------------------------------
; Take zonal mean (remember those NaNs)
; -------------------------------------
if (verbose) then print,'; Taking zonal mean ...'
;
ozono1=reform(ozono1,nlon1,nlat1*npress1*ntime)
ozono1_tmp=fltarr(nlat1*npress1*ntime*1L)
for ii=0L,nlat1*npress1*ntime*1L-1 do ozono1_tmp[ii]=mean(ozono1[*,ii],/nan)
; test=moment(ozono1[*,ii],maxmom=1,/nan)
; ozono1_tmp[ii]=test[0]
ozono1=reform(ozono1_tmp,nlat1,npress1,ntime)*1e6
;
; -------------------------------------------------------------------------
; Process near-surface NaNs by drawing down lowermost valid value in column
; -------------------------------------------------------------------------
if (verbose) then print,'; Processing near-surface NaNs ...'
;
nearsurlvp=500.
lowlvp=where(press1 ge nearsurlvp,complement=hilvp,nlowlvp)
if (nlowlvp gt 0 and nlowlvp lt npress1) then  begin  & $
  maxlolvp=where(press1 eq max(press1[lowlvp]),nmaxlo)  & $
  minlolvp=where(press1 eq min(press1[lowlvp]),nminlo)  & $
  maxhilvp=where(press1 eq max(press1[hilvp]),nmaxhi)  & $
  lvpstep=minlolvp[0]-maxhilvp[0]  & $
  ozon1lc=reform(ozono1[*,maxhilvp,*],nlat1*ntime)  & $
  checknan=where(finite(ozon1lc) eq 0.0,nmissn)  & $
  if (nmissn gt 0) then print, $
    '; NaNs encountered above level ',strtrim(string(nearsurlvp),2),' hPa!'  & $
  for ii=minlolvp[0],maxlolvp[0],lvpstep do begin  & $
    ozon1l=reform(ozono1[*,ii,*],nlat1*ntime)  & $
    missing_locs=where(finite(ozon1l) eq 0.0,nmiss)  & $
    if (nmiss gt 0) then begin  & $
      ozon1l[missing_locs] = ozon1lc[missing_locs] & $
      ozono1[*,ii,*]=reform(ozon1l,nlat1,1,ntime)  & $
    endif  & $
  ozon1lc=ozon1l  & $
  endfor  & $
endif  else print,'; Near-surface NaN filling inactive!'

; Check will only give output if NaNs or Negative values are detected
checkarray,ozono1,/outer,/negative

; ----------------------------------------------
; The horizontal interpolation takes place here:
; ----------------------------------------------
if (verbose) then print,'; Interpolating in the horizontal ...'
;
ozono=TRANSPOSE(ozono1, [2, 0, 1])
latit=lat1

; Deduce the southern most grid latitude and latitude spacing
if keyword_set(endgame) then begin
   dy=180.0/float(puntisuy)
   firsty=-90.0+(0.5*dy)
endif else begin
   dy=180.0/float(puntisuy-1)
   firsty=-90.0
endelse

; Calculate all the latitudes
newlatit=firsty+[FINDGEN(puntisuy)*dy]

nuovoozo=INTERPOLATE(ozono[*, *, *], FINDGEN(ntime), $
          FINDGEN(puntisuy)*(nlat1-1.)/(puntisuy-1.), $
          FINDGEN(npress1), /GRID)
ozohoriz=TRANSPOSE(nuovoozo, [1, 2, 0]) ; horiz. interp. ozone

; The SPARC climatology is read from an ascii file:
sparc_Tclim_file=dirnamesuppl+'/sparc-T-climo.ascii'
lspar=FINDGEN(41)*4.-80.                ; SPARC latitudes and pressure
pspar=10^(3.-(FINDGEN(33)/6.))          ; levels which end, alike
tspar=FLTARR(12, 41, 33)                ; Karen's, at 0.00464159 hPa
;
if (verbose) then print,'; Reading in the SPARC Temperature climatology ',sparc_Tclim_file
OPENR, liber, sparc_Tclim_file, /GET_LUN ; looks for a free unit
READF, liber, tspar                           ; SPARC climo is read in
CLOSE, liber
;
; need to interpolate SPARC climatology onto new ozone levels (SMO 08-09-09)
; The pspar levels turn out to be the same as Karen's first 33 levels
; and the rest of the code build on this.
tspar=reform(tspar,12*41,33)
tspar1=fltarr(12*41,npress1) ; pressure levels are as input ozone (SMO)
for ii=0,12*41-1 do tspar1[ii,*]=interpol(tspar[ii,*],alog(pspar),alog(press1))
tspar1=reform(tspar1,12,41,npress1)
tspar=tspar1
pspar=press1
;
; The SPARC zonal mean climatology is interpolated horizontally to the model 
; latitude grid spacing
spartoka=FINDGEN(puntisuy)*(size(lspar,/N_elements)-1.)/(puntisuy-1.)*90./80.
spartoka=spartoka-10./180.*spartoka(size(spartoka,/N_elements)-1)
; newlatit is like spartoka*4.-80.
sparctem=INTERPOLATE(tspar[*, *, *], FINDGEN(12), spartoka, FINDGEN(33), /GRID)
;
; Geopotential heights are estimated on the basis of the SPARC climo
; by integrating the hydrostatic equation in the vertical:
geosparc=FLTARR(12, puntisuy, 33) ; geopotential height array
geosparc[*, *, 0]= $
          (1./grav)*(Ride/Mair)*sparctem[*, *, 0]*ALOG(sealevpr/pspar[0])
FOR i=1, size(pspar,/N_elements)-1 DO BEGIN  & $
  geosparc[*, *, i]=geosparc[*, *, i-1]+ $
          (1./grav)*(Ride/Mair)* $ 
      0.5*(sparctem[*, *, i]+sparctem[*, *, i-1])*ALOG(pspar[i-1]/pspar[i])  & $
ENDFOR

; The file with the height levels is opened and the (theta) levels are read in
;
if (size(levlabel,/N_elements) gt 0) then $
  um_level_file=dirnamesuppl+'/O3_z_theta_'+nolev+levlabel+'.txt'  $
else um_level_file=dirnamesuppl+'/O3_z_theta_'+nolev+'.txt'
if (verbose) then print,'; Reading in the model height levels from ',um_level_file
;
heightum=FLTARR(livelliv)
OPENR, freie, um_level_file, /GET_LUN 
READF, freie, heightum
CLOSE, freie

ozonncdf=FLTARR(1, puntisuy, livelliv, ntime) ; O3, height grid.
; --------------------------------------------------------------
; The vertical interpolation takes place in the following lines:
; --------------------------------------------------------------
if (verbose) then print,'; Interpolating in the vertical ... '
FOR nmese=0, ntime-1 DO BEGIN  & $ ; Every month in the time series.
  nmois=ROUND((zeit[nmese]-FIX(zeit[nmese]))*12.-0.5)  & $
  ;nmois=fix(zeit[nmese] mod 12); SMO change
  FOR j=0, puntisuy-1 DO BEGIN  & $ ; See corresponding line of above FOR loops.
    FOR i=0, livelliv-1 DO BEGIN  & $
      l=livelliv-1-i    & $         ; See corresponding line of above FOR loops.
      upp=size(pspar,/N_elements)-1  & $
      WHILE ((geosparc[nmois, j, upp] GT heightum[l]) AND $
          (upp GT 0)) DO upp=upp-1  & $
      low=0  & $
      WHILE ((heightum[l] GT geosparc[nmois, j, low]) AND $
         (size(pspar,/N_elements)-1 GT low)) DO low=low+1  & $
      IF (upp NE low) THEN BEGIN  & $ ; See corresponding line of above FOR loops.
        upp=upp+1    & $              
        low=low-1    & $              
        ozonncdf[0, j, l, nmese]=ozohoriz[j, low, nmese]+ $   ; Linear
         (ozohoriz[j, upp, nmese]-ozohoriz[j, low, nmese])* $ ; interpol.
         (heightum[l]-geosparc[nmois, j, low])/ $
         (geosparc[nmois, j, upp]-geosparc[nmois, j, low])  & $
      ENDIF ELSE BEGIN  & $ ; Level l is above or below the range of pspar:
      ;; (SMO 09-09-09) scale O3 above 1hPa to zero.
        ozonncdf[0, j, l, nmese]=ozohoriz[j, low, nmese]  & $ ; closest Value.
      ;	ozonncdf[0, j, l, nmese]=ozohoriz[j, low, nmese] * $
      ;                   exp(-1.*abs(heightum[l]-heightum[low+1])/6950.)  & $
      ENDELSE  & $
    ENDFOR  & $
  ENDFOR  & $
ENDFOR
; ---------------------------------------
; The vertical interpolation is complete.
; ---------------------------------------

;
; -------------------------------------------------------------
; Either a climatology or the full time series are written out:
; -------------------------------------------------------------
IF (keyword_set(clim)) THEN BEGIN
  if (verbose) then print,'; Creating mean monthly climatology ... '
;
  ozocliml=FLTARR(1, puntisuy, livelliv, 12) ; O3 climatology:
  moncycle=[ROUND((zeit-FIX(zeit))*12.-0.5)]
; By this point assume that NaNs, MDIs have been screened out
  FOR nmois=0, 11 DO BEGIN  & $
    mon4mean=WHERE(moncycle EQ nmois,nmon4mean)  & $
    if (nmon4mean gt 0) then ozocliml[0, *, *, nmois] = $
      rebin(ozonncdf[0, *, *, mon4mean], 1, puntisuy, livelliv, 1)  & $
  ENDFOR
  zeitncdf=findgen(12)*30.+15.
  timeunit='days since 0000-01-01 00:00:00'
  timeorigin='01-JAN-0000:00:00:00'
  ozonncdf=ozocliml
;
ENDIF ELSE BEGIN
  if (verbose) then print,'; Preparing timeseries information ... '
;
  iniyear=FIX(zeit[0]) ; for example 1978
  ;; SMO change
  inimont=1+ROUND((zeit[0]-FIX(zeit[0]))*12.-0.5) ; for example 11
  ;inimont=zeit[0]+1
  namarrmon=['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC']
  namemont=namarrmon[inimont-1]
  timeunit='days since '+STRTRIM(STRING(iniyear), 2)+'-'+ $
          STRING(inimont, FORMAT='(I2.2)')+'-01 00:00:00'
  timeorigin='01-'+namemont+'-'+STRTRIM(STRING(iniyear), 2)+':00:00:00'
  zeitncdf=findgen(ntime)*30.+15.
ENDELSE
;
; -------------------------------------------------------------------------
; Rescale ozone by a specified amount and allow smear to 3D for LAM masters
; -------------------------------------------------------------------------
IF (size(lamlong,/N_elements) eq 0) THEN BEGIN
  ozonncdf=ozonncdf*multiply
  newlongi=360. ; This means that it is a zonal mean.
ENDIF ELSE BEGIN
  ozondims=size(ozonncdf,/dimensions)
  ozonncdf=rebin(ozonncdf,[lamlong,ozondims[1:3]])*multiply
; Calculate all the longitudes
  dx=360./lamlong
  if keyword_set(endgame) then firstx=0.5*dx else firstx=0.0
  newlongi=firstx+[FINDGEN(lamlong)*dx]
  sudno='3D'+sudno
ENDELSE

if (verbose) then print,'; Processing of data successfully completed.'
;
; ------------------------------------
; ------------------------------------
; Prepare OUTPUT information and data.
; ------------------------------------
; ------------------------------------
; Coordinate variables must keep the same name for the variable
; dimension and the variable itself:
namelong='longitude'
namelati='latitude'
nameheig='hybrid_ht'
nameozon='O3'
namezeit='t'

if (keyword_set(clim)) then stproc='M' else stproc='Y'
styear=string(fix(zeit[0]),format='(I4)')
if (fix(zeit[ntime-1]) ne fix(zeit[0])) then $
  styear=styear+'-'+string(fix(zeit[ntime-1]),format='(I4)')
namencdf=filestem+stproc+styear+'_'+sudno+nolev+colo3+'.nc'
if (verbose) then print,'; Prepare output to file ',namencdf
;
; ---------------------------
; The netcdf file is created:
; ---------------------------
zahldate=NCDF_CREATE(namencdf, /CLOBBER)
NCDF_CONTROL, zahldate, /FILL ; pre-filled with default fill values.

; ---------------------------------
; Variable [0] namelong='longitude'
; ---------------------------------
; The ozone data are zonal averages:
dimelong=NCDF_DIMDEF(zahldate, namelong, size(newlongi,/N_elements))
dimelong=NCDF_DIMID(zahldate,  namelong)
varilong=NCDF_VARDEF(zahldate, namelong, [dimelong], /FLOAT)
NCDF_ATTPUT, zahldate, varilong, 'units', 'degrees_east' 
NCDF_ATTPUT, zahldate, varilong, 'point_spacing', 'even'
; The above must be units in order to be correctly interpreted by xconv.
  
; --------------------------------
; Variable [1] namelati='latitude'
; --------------------------------
dimelati=NCDF_DIMDEF(zahldate, namelati, size(newlatit,/N_elements))
dimelati=NCDF_DIMID(zahldate,  namelati)
varilati=NCDF_VARDEF(zahldate, namelati, [dimelati], /FLOAT)
NCDF_ATTPUT, zahldate, varilati, 'units', 'degrees_north'
NCDF_ATTPUT, zahldate, varilati, 'point_spacing', 'even'

; ---------------------------------
; Variable [2] nameheig='hybrid_ht'
; ---------------------------------
dimehoeh=NCDF_DIMDEF(zahldate, nameheig, size(heightum,/N_elements))
dimehoeh=NCDF_DIMID(zahldate,  nameheig)
variheig=NCDF_VARDEF(zahldate, nameheig, [dimehoeh], /FLOAT)
NCDF_ATTPUT, zahldate, variheig, 'units', 'm'
NCDF_ATTPUT, zahldate, variheig, 'positive', 'up'
; The above attribute is recommended for the vertical axis

; -------------------------
; Variable [3] namezeit='t'
; -------------------------
; help,zeitwrite
dimezeit=NCDF_DIMDEF(zahldate, namezeit, size(zeitwrite,/N_elements))
dimezeit=NCDF_DIMID(zahldate,  namezeit)
varizeit=NCDF_VARDEF(zahldate, namezeit, [dimezeit], /FLOAT)
NCDF_ATTPUT, zahldate, varizeit, 'units', timeunit
NCDF_ATTPUT, zahldate, varizeit, 'calendar', '360_day'
NCDF_ATTPUT, zahldate, varizeit, 'time_origin', timeorigin

; --------------------------
; Variable [4] nameozon='O3'
; --------------------------
; If an ancillary needs be created, data have to be in mass mixing ratios:
ppmv2mmr=(1.0E-6*Mozo/Mair)
IF (keyword_set(massmix)) THEN begin  & $
  ozonncdf=ozonncdf*ppmv2mmr  & $
  ozounit='mass mixing ratio'   & $
ENDIF ELSE ozounit='ppmv'
;
variozon=NCDF_VARDEF(zahldate, nameozon, $
          [dimelong, dimelati, dimehoeh, dimezeit], /FLOAT)
NCDF_ATTPUT, zahldate, variozon, 'source', data_from
NCDF_ATTPUT, zahldate, variozon, 'name', nameozon
NCDF_ATTPUT, zahldate, variozon, 'title', 'Ozone '+ozounit
NCDF_ATTPUT, zahldate, variozon, 'long_name', nameozon+', '+ 'SPARC Ozone'
NCDF_ATTPUT, zahldate, variozon, 'units', 'kg kg^-1'

NCDF_ATTPUT, zahldate, /GLOBAL, 'source', $
'AC&C (http://www.igac.noaa.gov/ACandC.php) / SPARC (http://www.atmosp.physics.utoronto.ca/SPARC) / NCAS (http://ncas-climate.nerc.ac.uk/chris)'

NCDF_ATTPUT, zahldate, /GLOBAL, 'contact', $
'Scott Osprey (s.osprey@physics.ox.ac.uk), Chris Bell (--) High-top extension and Jean Francois Lamarque (lamar@ucar.edu), Bill Randel(randel@ucar.edu); Veronika Eyring (veronika.eyring@dlr.de) for original SPARC compilation'

NCDF_ATTPUT, zahldate, /GLOBAL, 'title', $
'High-Top extension of AC&C/SPARC Ozone Database for CMIP5'

NCDF_CONTROL, zahldate, /ENDEF
; ----------------------------------------------------
; Completed Define Mode, now move to Data mode Output.
; ----------------------------------------------------
help,ozonncdf
NCDF_VARPUT, zahldate, varilong, newlongi
NCDF_VARPUT, zahldate, varilati, newlatit
NCDF_VARPUT, zahldate, variheig, heightum
NCDF_VARPUT, zahldate, varizeit, zeitncdf
NCDF_VARPUT, zahldate, variozon, ozonncdf
NCDF_CLOSE, zahldate
; ----------------------------
; The netcdf file is complete:
; ----------------------------
if (verbose) then print,'; Your file has been written. Mischief managed!'
;
end
;
