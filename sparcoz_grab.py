# Python v2.7
#+
# NAME: sparcoz_grab
# PURPOSE: Extract SPARC(II) Ozone data from NetCDF file.
# CATEGORY: Datafile Reader
# CALLING SEQUENCE: sparcoz_grab,'NetCDF_filename',[...]
# INPUTS: filename=full name of netCDF file
# OPTIONAL INPUT PARAMETERS: years=array of years to subselect (default is all)
#                          : mdi2nan= True: replace missing data with NaN flag,
#                          :          False: do not identify missing data,
# OPTIONAL OUTPUT PARAMETERS: lonout=observation grid longitudes
#                           : latout=observation grid latitudes
#                           : levout=observation grid (pressure) levels
#                           : timeout=observation times (assumed 1 per month)
#                           : intervalsout=subselected times (see years)
#                           : ozoneout=ozone observations at subselected times
#                           : namesout=array of variable names
#                           : mdiout=value of missing data indicator (or NaN)
# SIDE EFFECTS: None observed
# RESTRICTIONS: Assume format close to that of example below
# PROCEDURES USED:
# MODIFICATION HISTORY: Created by Andrew Bushell, Mon Jul 18 17:21:57 2011
#                     : Modified by Andrew Bushell, 29 Jul 2011 (check time units)
#                     : Modified by Andrew Bushell, 08 Aug 2011 (NaN)
#                     : Ported to Python by Matthew Griffith Thu Oct 18 2018
#-
#

## Example File contains the following variables:
#   0 lon
#   1 lat
#   2 plev
#   3 time
#   4 O3
##
## Variable Dimensions:
#   0 time, LEN =          120(Unlimited dim)
#   1 plev, LEN =           24
#   2 lat,  LEN =           37
#   3 lon,  LEN =           72
##
## Variables:
#lon (FLOAT ) Dimension Ids = [ 3 ]
##   Attribute: standard_name= longitude
##   Attribute: axis= X
##   Attribute: long_name= Longitude
##   Attribute: units= degrees_east
#lat (FLOAT ) Dimension Ids = [ 2 ]
##   Attribute: standard_name= latitude
##   Attribute: axis= Y
##   Attribute: long_name= Latitude
##   Attribute: units= degrees_north
#plev (FLOAT ) Dimension Ids = [ 1 ]
##   Attribute: standard_name= air_pressure
##   Attribute: positive= down
##   Attribute: axis= Z
##   Attribute: long_name= Pressure
##   Attribute: units= hPa
#time (FLOAT ) Dimension Ids = [ 0 ]
##   Attribute: long_name= time
##   Attribute: units= months since 1850-01-01 00:00:00
##   Attribute: standard_name= time
##   Attribute: calendar= standard
##   Attribute: axis= T
#O3 (FLOAT ) Dimension Ids = [ 3 2 1 0 ]
##   Attribute: cell_methods= time:mean (interval: 1 months)
##   Attribute: units= mole mole^-1
##   Attribute: standard_name= mole_fraction_of_ozone_in_air
##   Attribute: _FillValue=   1.00000e+36
##   Attribute: long_name= O3

def sparcoz_grab(filename, mdi2nan=False, years=[]):
    
    from netCDF4 import Dataset
    import numpy as np
    
    ncfile = Dataset(filename, 'r')

    varcheck = len(ncfile.variables)
    if (varcheck != 5):    
        print '; NetCDF file not in standard Ozone data format!'
    # --------------------------
    # Read in Dimension metadata
    # --------------------------
    vloninq = 'longitude'
    nlon = ncfile.variables[vloninq].size
    lon1d = ncfile.variables[vloninq][:]
    #
    vlatinq = 'latitude'
    nlat = ncfile.variables[vlatinq].size
    lat1d = ncfile.variables[vlatinq][:]
    #
    vlevinq = 'pressure'
    nlev = ncfile.variables[vlevinq].size
    lev1d = ncfile.variables[vlevinq][:]
    #
    vtiminq = 't'
    nyrange = np.size(years)
    time1d = ncfile.variables[vtiminq][:]
    if (nyrange > 0):
        if 'mon' in ncfile.variables[vtiminq].units:
            monfac = 1.
        elif ('day' in ncfile.variables[vtiminq].units) and '360' in ncfile.variables[vtiminq].calendar:    
            monfac = 30.
        else:
            print 'Unexpected Time Intervals! ' + ncfile.variables[vtiminq].units
        
        intervals = np.zeros(12 * nyrange, dtype = np.float32)
        
        for i in np.arange(nyrange):
            intervals[12 * i:((12 * i) + 12)] = monfac * (12 * (years[i] - 1850) + np.arange(12))
    else:    
        intervals = ncfile.variables[vtiminq][:]
    
    noztim = np.size(intervals)
    
    if np.max(intervals) < np.min(time1d):    
        print 'Applied Time window is before interval ', np.min(time1d), '!'
    if np.min(intervals) > np.max(time1d):    
        print 'Applied Time window is after interval ', np.max(time1d), '!'
    # ---------------------------------------------
    # Read in Ozone fields from selected years only
    # ---------------------------------------------
    ozone = np.zeros([noztim, nlev, nlat, nlon], dtype = np.float32)
    vozinq = 'O3'
    if mdi2nan:    
        ozmdi = ncfile.variables['O3'].missing_value
    else:    
        ozmdi = np.nan
    for i in np.arange(noztim):
        time1d_ind = (time1d >= intervals[i]).nonzero()[0]
        ntim = np.size(time1d_ind)
        if (ntim > 0):
            timoff = np.min(time1d_ind)
            oz3d = ncfile.variables['O3'][timoff,:,:,:]
            ozone[i, 0:nlev, 0:nlat, 0:nlon] = oz3d

    # Conform to original IDL netcdf import structure
    ozone = np.transpose(ozone, (3,2,1,0))
    
    ncfile.close()
    # --------------------------------------------
    # Option to replace missing data with NaN flag
    # --------------------------------------------
    if mdi2nan:
        ozone1d = np.reshape(ozone, nlon * nlat * nlev * noztim)
        missing_locs_TF = np.array([a or b for a, b in zip(ozone1d == ozmdi, np.isnan(ozone1d))])
        missing_locs = missing_locs_TF.nonzero()[0]
        nmiss = np.size(missing_locs)
        if (nmiss > 0):
            ozone1d[missing_locs] = np.nan
            ozone = np.reshape(ozone1d, (nlon, nlat, nlev, noztim))
            ozmdi = np.nan
    # -------------
    # Output fields
    # -------------
    lonout = lon1d
    latout = lat1d
    levout = lev1d
    timeout = time1d
    intervalsout = intervals
    ozoneout = ozone
    namesout = np.array([vloninq, vlatinq, vlevinq, vtiminq, vozinq])
    mdiout = ozmdi
    
    return lonout, latout, levout, timeout, intervalsout, ozoneout, namesout, mdiout
