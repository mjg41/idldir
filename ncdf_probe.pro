; IDL Version 6.4 (linux x86 m32)
; Journal File for magmp@eld058
; Working directory: /net/home/h05/magmp/idldir
; Date: Mon Sep  3 14:56:16 2007
 
; Test program to probe contents of a NetCDF file (see IDL help)

pro ncdf_probe,filename

fileid=ncdf_open(filename,/NOWRITE)
resume=ncdf_inquire(fileid)
;
; List contents
print,'; File contains the following variables: '
for i=0,resume.Nvars-1 do begin  & $
 vname=ncdf_varinq(fileid,i)  & $
 print,FORMAT='(I4," ",A)',i,vname.name  & $
endfor
;
; Show user size of each dimension
print,'; '
print,'; Variable Dimensions: '
;
for i=0,resume.ndims-1 do begin
  ncdf_diminq,fileid,i,name,size
  if i EQ resume.recdim  then   $
    print,FORMAT='(I4," ",A,", LEN = ",I, "(Unlimited dim) ")', i, name, size  $
  else   $
    print,FORMAT='(I4," ",A,", LEN = ",I)', i, name, size 
endfor
;
; Provide variable descriptions
print,'; '
print,'; Variables: '
;
for i=0,resume.nvars-1 do begin
; Get information about the variable
  info=ncdf_varinq(fileid,i)
  FmtStr='(A," (",A," ) Dimension Ids = [ ", 10(I0," "),$)'
  print,FORMAT=FmtStr,info.name,info.datatype,info.dim[*]
  print, ']'
;
; Get attributes associated with the variable
  for j=0,info.natts-1 do begin  & $
    attname = ncdf_attname(fileid,i,j)  & $
    ncdf_attget,fileid,i,attname,attvalue  & $
    print,';   Attribute: ', attname, '= ', string(attvalue)  & $
  endfor
endfor
;
ncdf_close,fileid
;
end
