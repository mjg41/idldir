; IDL Version 7.1.1 (linux x86 m32)
;+
; NAME: sparcoz_grab
; PURPOSE: Extract SPARC(II) Ozone data from NetCDF file.
; CATEGORY: Datafile Reader
; CALLING SEQUENCE: sparcoz_grab,'NetCDF_filename',[...]
; INPUTS: filename=full name of netCDF file
; OPTIONAL INPUT PARAMETERS: year=array of years to subselect (default is all)
;                          : mdi2nan= +1 replace missing data with NaN flag,
;                          :           0 do not identify missing data,
;                          :          -1 identify missing data via mdiout. 
; OPTIONAL KEYWORD PARAMETERS : verbose - printout from ncdf_probe,filename
; OPTIONAL OUTPUT PARAMETERS: lonout=observation grid longitudes
;                           : latout=observation grid latitudes
;                           : levout=observation grid (pressure) levels
;                           : timeout=observation times (assumed 1 per month)
;                           : intervalsout=subselected times (see year)
;                           : ozoneout=ozone observations at subselected times
;                           : namesout=array of variable names
;                           : mdiout=value of missing data indicator (or NaN)
; SIDE EFFECTS: None observed
; RESTRICTIONS: Assume format close to that of example below 
; PROCEDURE:
; MODIFICATION HISTORY: Created by Andrew Bushell, Mon Jul 18 17:21:57 2011
;                     : Modified by Andrew Bushell, 29 Jul 2011 (check time units)
;                     : Modified by Andrew Bushell, 08 Aug 2011 (NaN)
;-
;
 
;; Example File contains the following variables: 
;   0 lon
;   1 lat
;   2 plev
;   3 time
;   4 O3
;; 
;; Variable Dimensions: 
;   0 time, LEN =          120(Unlimited dim) 
;   1 plev, LEN =           24
;   2 lat,  LEN =           37
;   3 lon,  LEN =           72
;; 
;; Variables: 
;lon (FLOAT ) Dimension Ids = [ 3 ]
;;   Attribute: standard_name= longitude
;;   Attribute: axis= X
;;   Attribute: long_name= Longitude
;;   Attribute: units= degrees_east
;lat (FLOAT ) Dimension Ids = [ 2 ]
;;   Attribute: standard_name= latitude
;;   Attribute: axis= Y
;;   Attribute: long_name= Latitude
;;   Attribute: units= degrees_north
;plev (FLOAT ) Dimension Ids = [ 1 ]
;;   Attribute: standard_name= air_pressure
;;   Attribute: positive= down
;;   Attribute: axis= Z
;;   Attribute: long_name= Pressure
;;   Attribute: units= hPa
;time (FLOAT ) Dimension Ids = [ 0 ]
;;   Attribute: long_name= time
;;   Attribute: units= months since 1850-01-01 00:00:00
;;   Attribute: standard_name= time
;;   Attribute: calendar= standard
;;   Attribute: axis= T
;O3 (FLOAT ) Dimension Ids = [ 3 2 1 0 ]
;;   Attribute: cell_methods= time:mean (interval: 1 months)
;;   Attribute: units= mole mole^-1
;;   Attribute: standard_name= mole_fraction_of_ozone_in_air
;;   Attribute: _FillValue=   1.00000e+36
;;   Attribute: long_name= O3
 
pro sparcoz_grab,filename,verbose=verbose,mdi2nan=mdi2nan,year=year,$
lonout=lonout,latout=latout,levout=levout,timeout=timeout,intervalsout=intervalsout,$
ozoneout=ozoneout,namesout=namesout,mdiout=mdiout

fileid=ncdf_open(filename,/NOWRITE)
if (keyword_set(verbose) gt 0) then ncdf_probe,filename
inquest=ncdf_inquire(fileid)
if (inquest.nvars ne 5) then print,'; NetCDF file not in standard Ozone data format!'
; --------------------------
; Read in Dimension metadata
; --------------------------
vloninq=ncdf_varinq(fileid,0)
lopid=ncdf_varid(fileid,vloninq.name)
ncdf_varget,fileid,lopid,lon1d
nlon=size(lon1d,/N_elements)
;
vlatinq=ncdf_varinq(fileid,1)
lapid=ncdf_varid(fileid,vlatinq.name)
ncdf_varget,fileid,lapid,lat1d
nlat=size(lat1d,/N_elements)
;
vlevinq=ncdf_varinq(fileid,2)
plpid=ncdf_varid(fileid,vlevinq.name)
ncdf_varget,fileid,plpid,lev1d
nlev=size(lev1d,/N_elements)
;
vtiminq=ncdf_varinq(fileid,3)
tpid=ncdf_varid(fileid,vtiminq.name)
ncdf_varget,fileid,tpid,time1d
; ncdf_attget,fileid,tpid,ncdf_attname(fileid,tpid,0),timunits
ncdf_attget,fileid,tpid,'units',timunits
; ncdf_attget,fileid,tpid,ncdf_attname(fileid,tpid,1),timcal
ncdf_attget,fileid,tpid,'calendar',timcal
nyrange=size(year,/N_elements)
if (nyrange gt 0) then begin  & $  ; Check time units (assume month intervals)
  if (STRCMP(string(timunits), 'mon', 3, /FOLD_CASE)) then monfac=1. else  $
  if (STRCMP(string(timunits), 'day', 3, /FOLD_CASE) and $
      STRCMP(string(timcal), '360', 3))  then monfac=30. else $
    print,'; Unexpected Time Intervals! '+string(timunits) & $
  intervals=fltarr(12*nyrange)  & $
  for i=0,nyrange-1 do begin  & $
    intervals[12*i:(12*i)+11]=monfac*(12*(year[i]-1850)+indgen(12))  & $
  endfor  & $
endif  $
else intervals=time1d
noztim=size(intervals,/N_elements)

if (max(intervals) lt min(time1d)) then print,'; Applied Time window is before interval ',min(time1d) ,'!'
if (min(intervals) gt max(time1d)) then print,'; Applied Time window is after interval ',max(time1d) ,'!'
; ---------------------------------------------
; Read in Ozone fields from selected years only
; ---------------------------------------------
ozcount=[nlon,nlat,nlev,1]
ozone=fltarr(nlon,nlat,nlev,noztim)
vozinq=ncdf_varinq(fileid,4)
ozpid=ncdf_varid(fileid,vozinq.name)
if (keyword_set(mdi2nan) gt 0) then  $
  ncdf_attget,fileid,ozpid,'missing_value',ozmdi  else  ozmdi=!values.f_nan
for i=0,noztim-1 do begin  & $
  timoff=min(where(time1d ge intervals[i],ntim))  & $
  if (ntim gt 0)  then begin  & $
    ozoff=[0,0,0,timoff]  & $
    ncdf_varget,fileid,ozpid,oz3d,offset=ozoff,count=ozcount  & $
    ozone[0:nlon-1,0:nlat-1,0:nlev-1,i]=oz3d  & $
  endif  & $
endfor
;
ncdf_close,fileid
; --------------------------------------------
; Option to replace missing data with NaN flag
; --------------------------------------------
if (keyword_set(mdi2nan) gt 0) then begin  & $
  if (mdi2nan gt 0) then begin  & $
    ozone1d=reform(ozone,nlon*nlat*nlev*noztim)  & $
    missing_locs=where(ozone1d eq ozmdi,nmiss)  & $
    if (nmiss gt 0) then ozone1d[missing_locs]=!values.f_nan  & $
    ozone=reform(ozone1d,nlon,nlat,nlev,noztim)  & $
    ozmdi=!values.f_nan  & $
  endif  & $
endif
; -------------
; Output fields
; -------------
lonout=lon1d
latout=lat1d
levout=lev1d
timeout=time1d
intervalsout=intervals
ozoneout=ozone
namesout=[vloninq.name,vlatinq.name,vlevinq.name,vtiminq.name,vozinq.name]
mdiout=ozmdi
;
end
;
