;+
; NAME: checkarray 
; PURPOSE: Perform checks on data in an array.
; CATEGORY: Diagnostic.
; CALLING SEQUENCE: checkarray,array,[...]
; INPUTS: array= data array to be checked
; OPTIONAL INPUT PARAMETERS: mdi= a missing data indicator to check for
;                          : crynan= +1 Print even if no NaNs are detected,
;                                     0 Print only if NaNs are detected,
;                                    -1 Quiet even if NaNs are detected,
;                          : negative= +1 check, print if negatives detected
;                                       0 no check on negative values
;                                      -1 check, print even if no negatives detected
; OPTIONAL KEYWORD PARAMETERS : outer - run check in loop over final dimension
; OPTIONAL OUTPUT PARAMETERS: 
; SIDE EFFECTS: None observed
; RESTRICTIONS: 
; PROCEDURE:
; MODIFICATION HISTORY: Created by Andrew Bushell, Fri Aug 12 12:22:00 2011
;-
;
pro checkarray,array,outer=outer,crynan=crynan,negative=negative,mdi=mdi

sizarray=size(array)
ndims=sizarray[0]
nfdim=sizarray[ndims]
nelem=sizarray[ndims+2]
;
if (keyword_set(crynan) eq 0) then crynan=0
lnantprn=(crynan ge 0)
lnanfprn=(crynan gt 0)
;
lnegtprn=(keyword_set(negative))
if (lnegtprn) then lnegfprn=(negative lt 0) else lnegfprn=lnegtprn
;
if (keyword_set(outer) eq 0) then  begin  & $
; Checks made over entire array
  nchelem=nelem  & $
  check=reform(array,nchelem)  & $
; -----------------------
; locate presence of NaNs
; -----------------------
  nancheck=where(finite(check) eq 0.0,complement=realcheck,nchnan)  & $
  if (nchnan ge nchelem) then  begin  & $
; --------------------
; All elements are NaN
; --------------------
    print,'; All elements are NaN: Total='+strtrim(string(nchnan),2)  & $
  endif  else  begin  & $
; -------------------------
; Some elements are not NaN
; -------------------------
    if (nchnan gt 0) then  begin  & $
      check=check[realcheck]  & $
      if (lnantprn) then $
        print,'; NaNs: ',nchnan,'; Min: ',min(check),'; Max: ',max(check)  & $
    endif  else  if (lnanfprn) then $
      print,'; NaNs: ',nchnan,'; Min: ',min(check),'; Max: ',max(check)  & $
    stultim=' ; Min: '+strtrim(string(min(check)),2)+ $
            ' ; Max: '+strtrim(string(max(check)),2)  & $
;   ----------------------------
;   locate presence of Negatives
;   ----------------------------
    if (keyword_set(negative) gt 0) then  begin  & $
      negcheck=where(check lt 0.0,nchneg)  & $
      if ((nchneg gt 0) and (lnegtprn)) then $
        print,'; Negatives: ',strtrim(string(nchneg),2),stultim  $
      else  if ((nchneg eq 0) and (lnegfprn)) then $
        print,'; Negatives: ',strtrim(string(nchneg),2),stultim  & $
    endif  & $
;   -------------------------------
;   locate presence of Missing Data
;   -------------------------------
    if (size(mdi,/N_elements) eq 1) then  begin  & $
      mdicheck=where(check eq mdi,nchmdi) & $
      print,'; Missing Data: ',strtrim(string(nchmdi),2),stultim  & $
    endif  & $
;
  endelse
;
endif else  begin  & $
; Checks made within individual loop steps over final dimension of array
; (commonly time). NB can transpose matrix before procedure call to select alt.
  nchelem=nelem/nfdim  & $
  check2d=reform(reform(array,nelem),nchelem,nfdim)  & $
  for i=0,(nfdim-1) do begin  & $
    stloopi='; i: '+strtrim(string(i),2)+'  '  & $
    check=check2d[*,i]  & $
;   -----------------------
;   locate presence of NaNs
;   -----------------------
    nancheck=where(finite(check) eq 0.0,complement=realcheck,nchnan)  & $
    if (nchnan ge nchelem) then  begin  & $
;   --------------------
;   All elements are NaN
;   --------------------
      print,stloopi,'; All elements are NaN: Total='+strtrim(string(nchnan),2)  & $
    endif  else  begin  & $
;   -------------------------
;   Some elements are not NaN
;   -------------------------
      if (nchnan gt 0) then  begin  & $
        check=check[realcheck]  & $
        if (lnantprn) then $
          print,stloopi,'; NaNs: ',nchnan,'; Min: ',min(check),'; Max: ',max(check)  & $
      endif  else  if (lnanfprn) then $
        print,stloopi,'; NaNs: ',nchnan,'; Min: ',min(check),'; Max: ',max(check)  & $
      stultim=' ; Min: '+strtrim(string(min(check)),2)+ $
              ' ; Max: '+strtrim(string(max(check)),2)  & $
;     ----------------------------
;     locate presence of Negatives
;     ----------------------------
      if (keyword_set(negative) gt 0) then  begin  & $
        negcheck=where(check lt 0.0,nchneg)  & $
        if ((nchneg gt 0) and (lnegtprn)) then $
          print,stloopi,'; Negatives: ',strtrim(string(nchneg),2),stultim  $
        else  if ((nchneg eq 0) and (lnegfprn)) then $
          print,stloopi,'; Negatives: ',strtrim(string(nchneg),2),stultim  & $
      endif  & $
;     -------------------------------
;     locate presence of Missing Data
;     -------------------------------
      if (size(mdi,/N_elements) eq 1) then  begin  & $
        mdicheck=where(check eq mdi,nchmdi) & $
        print,stloopi,'; Missing Data: ',strtrim(string(nchmdi),2),stultim  & $
      endif  & $
;
    endelse
;
  endfor  & $
;  
endelse
;
; -------------
; Output fields
; -------------
;
end
;
