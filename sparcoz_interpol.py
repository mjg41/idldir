# Python v2.7
#+
# NAME: sparcoz_interpol
# PURPOSE: Process SPARC(II) Ozone data from NetCDF file and write to NetCDF.
# CATEGORY: Data Processing
# CALLING SEQUENCE:
# sparcoz_interpol,'NetCDF_input_ozone','NetCDF_outdir',nmodlev,nmodgrid,[...]
# INPUTS: input_ozone= full /directory/name of netCDF file from the Ozone dataset
#       : outdir= '/directory/filename_start' (year/grid descriptors appended)
#       : inlev= integer, number of model levels onto which to interpolate
#       : ingrid= integer, specifies [half-zonal (96==N96), meridional] resolution
#                 components of horizontal model grid onto which to interpolate
#                 data.
#                 Note that if routine recognizes grid it is not necessary to
#                 specify the meridional resolution component.
# OPTIONAL INPUT PARAMETERS: years= array of years to subselect (default is all)
#                          : datanote= string, note on source of the data
#                          : indir= directory containing input files
#                          : levlabel= string to differentiate equal numlevels, e.g. '100km'
#                          : multiply= multiply the ozone by a set amount
#                          : lamlong= number of longitudes for LAM 3D smearing
# OPTIONAL KEYWORD PARAMETERS : verbose - stop printout from ncdf_probe, etc.
#                             : massmix - sets data to be in mass mixing ratio
#                             :     (used when creating an ancillary from data)
#                             : clim - sets creation of mean climatology from
#                             :        timeseries
#                             : endgame - map onto grid offest from poles
# OPTIONAL OUTPUT PARAMETERS:
# SIDE EFFECTS: None observed
# RESTRICTIONS: Format assumptions (e.g. that unprocessed data is on pressure)
# PROCEDURES USED: sparcoz_grab
# MODIFICATION HISTORY: Created by Andrew Bushell, Thu Jul 28 16:54:32 2011
#                       Follows steps in interpol_o3_cmip5_v1.09.pro
#            [ Authors: Mauro Dall'Amico, Dep of Meteorology, Reading, 2007-Feb]
#            [ Scott Osprey, Chris Bell, updated for new SPARC ozone (28-08-09)]
#                     : Modified by Andrew Bushell, 12 Aug 2011 (tidy code)
#                     : Modified by Dan Copsey, 16 Mar 2012 (support ENDGame)
#                     : Modified by Andrew Bushell, 08 Jul 2015 (LAM '3D' option)
#                     : Ported to Python by Matthew Griffith Thu Oct 18 2018
#-
#

def sparcoz_interpol(input_ozone, outdir, inlev, ingrid, verbose=False, massmix=True, clim=True, \
                     endgame=True, years=[], datanote='S.Osprey rebuild of SPARC(II)', \
                     indir='/Users/mjg41/Documents/PhD/Ancillary_Creation/ozone/CMIP5/raw/', \
                     levlabel=[], multiply=1.0, lamlong=[]):
    
    import numpy as np
    from scipy.interpolate import interp1d
    from netCDF4 import Dataset
    from sparcoz_grab import sparcoz_grab
    from progress.bar import IncrementalBar

    # ----------------------------------------------------------------------
    # Specify desired horizonal, vertical resolutions of interpolated data:
    # ----------------------------------------------------------------------

    if (np.size(ingrid) < 2):
        if ingrid[0] == 24:
            puntisuy = 37
        elif ingrid[0] == 48:
            puntisuy = 73
        elif ingrid[0] == 96:
            puntisuy = 145
        elif ingrid[0] == 216:
            puntisuy = 325
        elif ingrid[0] == 320:
            puntisuy = 481
        elif ingrid[0] == 512:
            puntisuy = 769
        elif ingrid[0] == 768:
            puntisuy = 1153
        else:    
            raise Exception('Error, unexpected horizontal resolution!')
        if endgame:
            puntisuy = puntisuy - 1 ;
    else:    
        puntisuy = ingrid[1]
    
    puntisux = 2 * ingrid[0]
    
    livelliv = inlev
    # if ((inlev == 38) + (inlev == 60) + (inlev == 70) + (inlev == 85) + (inlev == 88) + (inlev == 120) == 0):    
    #     print 'Warning unexpected vertical resolution: check you have a levels file!'
    
    # ---------------------------------------------------------------
    # Title indication of linear interpolation and O3 above model lid
    # having been chopped off:
    # ---------------------------------------------------------------
    
    if (np.size(levlabel) > 0):    
        colo3 = '_' + levlabel + '_li-ch'
    else:    
        colo3 = '_li-ch'
    sudno = 'N' + str(ingrid[0])
    nolev = 'L' + str(inlev)
    # Note in the source attribute of the final netcdf file:
    if clim:    
        note = 'Climatology from '
    else:    
        note = 'Time series, '
    data_from = note + datanote
    #
    # The following constants are not to be changed throughout the program:
    Navo = 6.022e+4     # (10^22 molecules kMol^-1)
    grav = 9.80665      # (m s^-2) same value as for the UM 6.1
    Ride = 8314.0       # (J kg^-1 K^-1)
    Mair = 28.966       # (kg kMol^-1) as in the UM 6.1, see M_AIR
    sealevpr = 1013.25  # (hPa) and is assumed constant at all latitudes
    Mozo = 48.0         # (kg kMol^-1)
    
    # ------------------------------------------------
    # Read in a selected sub-period of input data file
    # and replace MDI by NaNs
    # ------------------------------------------------
    
    lon1, lat1, press1, timefull, time1, ozono1, _, oz1mdi = sparcoz_grab(input_ozone, mdi2nan=True, years=years)
    
    nt1 = np.size(time1)
    timint = time1[1] - time1[0]
    daycount = timint > 1.
    # Allow for sudden switch to counting time in (360-calendar) days, not months!
    # Note that zeit is counted in years, whatever the input data.
    if daycount:    
        zeit = 1850. + (time1 + 15.0) / 360.
    else:    
        zeit = 1850. + (time1 + 0.5) / 12.
    #-----

    nlat1 = np.size(lat1)
    nlon1 = long(np.size(lon1))
    npress1 = np.size(press1)
    ntime = np.size(zeit)
    
    # -------------------------------------
    # Take zonal mean (remember those NaNs)
    # -------------------------------------
    
    if verbose:    
        print 'Taking zonal mean ...'

    ozono1 = np.nanmean(ozono1, axis=0, dtype = np.float32)*1.0e6
    # Mutliply by 1.0e6 here so as not to have to worry about calculation errors
    # We multiply by 1.0e-6 in the Variable [4] definition

    # -------------------------------------------------------------------------
    # Process near-surface NaNs by drawing down lowermost valid value in column
    # -------------------------------------------------------------------------
    
    if verbose:    
        print 'Processing near-surface NaNs ...'
    #
    nearsurlvp = 500.
    lowlvp = (press1 >= nearsurlvp).nonzero()[0]
    nlowlvp = np.size(lowlvp)
    # Get complement using mask
    mask = np.ones(npress1, dtype=bool)
    mask[(lowlvp)] = False
    hilvp = np.arange(npress1)[mask]
    if ((nlowlvp > 0) and (nlowlvp < npress1)):
        maxlolvp = (press1 == np.max(press1[lowlvp])).nonzero()[0]
        nmaxlo = np.size(maxlolvp)
        minlolvp = (press1 == np.min(press1[lowlvp])).nonzero()[0]
        nminlo = np.size(minlolvp)
        maxhilvp = (press1 == np.max(press1[hilvp])).nonzero()[0]
        nmaxhi = np.size(maxhilvp)
        lvpstep = minlolvp[0] - maxhilvp[0]
        ozon1lc = np.reshape(ozono1[:,maxhilvp,:], nlat1 * ntime)
        checknan = np.size(np.argwhere(np.isnan(ozon1lc)))
        if (checknan > 0):    
            print 'NaNs encountered above level ', str(nearsurlvp), ' hPa!'
        for ii in np.arange(minlolvp[0], (maxlolvp[0] + lvpstep), lvpstep):
            ozon1l = np.reshape(ozono1[:,ii,:], nlat1 * ntime)
            missing_locs = np.argwhere(np.isnan(ozon1l))
            nmiss = np.size(missing_locs)
            if (nmiss > 0):
                ozon1l[missing_locs] = ozon1lc[missing_locs]
                ozono1[:,ii,:] = np.reshape(ozon1l, (nlat1, ntime))
            ozon1lc = ozon1l
    else:    
        print 'Near-surface NaN filling inactive!'
    
    # Check will only give error if NaNs are detected
    if np.isnan(ozono1).any():
        raise Exception('Left over NaNs!')
    
    # ----------------------------------------------
    # The horizontal interpolation takes place here:
    # ----------------------------------------------
    
    if verbose:    
        print 'Interpolating in the horizontal ...'
    
    latit = lat1
    
    # Deduce the southern most grid latitude and latitude spacing
    if endgame:    
        dy = 180.0 / puntisuy
        firsty = -90.0 + (0.5 * dy)
    else:    
        dy = 180.0 / (puntisuy - 1.)
        firsty = -90.0
    
    # Calculate all the latitudes
    newlatit = firsty + np.arange(puntisuy) * dy
    # Create interpolating function and do interpolation
    interp_lat = interp1d(latit, ozono1, axis=0)
    ozohoriz = interp_lat(newlatit) # horiz. interp. ozone
    
    # The SPARC climatology is read from an ascii file:
    sparc_Tclim_file = indir + 'sparc-T-climo.ascii'
    lspar = np.arange(-80.,80.001,4., dtype = np.float32)           # SPARC latitudes and pressure
    pspar = 10 ** (3. - (np.arange(33., dtype = np.float32) / 6.))  # levels which end, alike
                                                                    # Karen's, at 0.00464159 hPa
    #
    if verbose:    
        print 'Reading in the SPARC Temperature climatology ', sparc_Tclim_file
    
    f = open(sparc_Tclim_file, 'r')
    data = f.read()                        # SPARC climo is read in
    f.close()
    data = data.split()
    for i in np.arange(len(data)):
		data[i] = np.float32(data[i])

    # need to interpolate SPARC climatology onto new ozone levels (SMO 08-09-09)
    # The pspar levels turn out to be the same as Karen's first 33 levels
    # and the rest of the code build on this.
    tspar = np.reshape(data, (33, 41, 12))
    tspar = np.swapaxes(tspar,0,2) # Swap to (12, 41, 33) shape
    interp_press = interp1d(np.log(pspar), tspar, axis=2)
    tspar = interp_press(np.log(press1))
    pspar = press1
    #
    # The SPARC zonal mean climatology is interpolated horizontally to the model
    # latitude grid spacing
    spartoka = np.arange(puntisuy, dtype = np.float32) * (lspar.size - 1.) / (puntisuy - 1.) * 90. / 80.
    spartoka = spartoka - 10. / 180. * spartoka[spartoka.shape[0] - 1]
    # newlatit is like spartoka*4.-80.
    interp_lspar = interp1d(lspar, tspar, axis=1)
    sparctem = interp_lspar(spartoka)
    #
    # Geopotential heights are estimated on the basis of the SPARC climo
    # by integrating the hydrostatic equation in the vertical:
    geosparc = np.zeros((12, puntisuy, 31), dtype = np.float32) # geopotential height array
    geosparc[:,:,0] = (1. / grav) * (Ride / Mair) * sparctem[:,:,0] * np.log(sealevpr / pspar[0], dtype = np.float32)
    for i in np.arange(1, pspar.size):
        geosparc[:,:,i] = geosparc[:,:,i-1] + (1. / grav) * (Ride / Mair) * 0.5 * \
		                  (sparctem[:,:,i] + sparctem[:,:,i-1]) * \
		                  np.log(pspar[i - 1] / pspar[i], dtype = np.float32)
    
    # The file with the height levels is opened and the heights are read in

    if len(levlabel) > 0:    
        um_level_file = indir + 'O3_z_theta_' + nolev + '_' + levlabel + '.txt'
    else:    
        um_level_file = indir + 'O3_z_theta_' + nolev + '.txt'
    if verbose:    
        print 'Reading in the model height levels from ', um_level_file
    #

    g = open(um_level_file, 'r')
    heightum = g.read()                        # Heights read in
    g.close()
    heightum = heightum.replace('\n','')
    heightum = heightum.split(',')
    for i in np.arange(len(heightum)):
		heightum[i] = np.float32(heightum[i])

    ozonncdf = np.zeros((1, puntisuy, livelliv, ntime), dtype = np.float32) # O3, height grid.
    
    # --------------------------------------------------------------
    # The vertical interpolation takes place in the following lines:
    # --------------------------------------------------------------
    
    if verbose:
        print 'Interpolating in the vertical ... '
    # Loop over every month in time series
    bar = IncrementalBar('Vert Interp',max=ntime*puntisuy, suffix='%(percent)d%%')
    for nmese in np.arange(ntime):
		# Find month, Jan = 0, Dec = 11
        nmois = np.int(zeit[nmese]*12%12)
        # Loop over every latitude
        for j in np.arange(puntisuy):
			# Loop over every height level
            bar.next()
            for i in np.arange(livelliv):
                # Start from uppermost height level and work down
                l = livelliv - 1 - i
                # Initialise maximal and minimal indices for range of pspar
                upp = pspar.size - 1
                low = 0
                while ((geosparc[nmois, j, upp] > heightum[l]) and (upp > 0)):
                    upp = upp - 1
                while ((heightum[l] > geosparc[nmois, j, low]) and (low < (pspar.size - 1))):
                    low = low + 1
                if (upp != low):
                    upp = upp + 1
                    low = low - 1
                    # Linearly interpolate onto height corresponding to level l
                    # using pre-exisiting values from ozone data
                    ozonncdf[0, j, l, nmese] = ozohoriz[j, low, nmese] + \
											(ozohoriz[j, upp, nmese] - ozohoriz[j, low, nmese]) * \
											(heightum[l] - geosparc[nmois, j, low]) / \
											(geosparc[nmois, j, upp] - geosparc[nmois, j, low])
                else:
					# Level l is above or below range of ozone data (which has
					# pressures pspar), so just copy heighest matched level
                    ozonncdf[0, j, l, nmese] = ozohoriz[j, low, nmese]
                    # ozonncdf[0, j, l, nmese]=ozohoriz[j, low, nmese] * exp(-1.*abs(heightum[l]-heightum[low+1])/6950.)
    bar.finish()
    # ---------------------------------------
    # The vertical interpolation is complete.
    # ---------------------------------------
    
    # -------------------------------------------------------------
    # Either a climatology or the full time series are written out:
    # -------------------------------------------------------------
    
    if clim:    
        if verbose:    
            print 'Creating mean monthly climatology ... '
        
        ozocliml = np.zeros((1, puntisuy, livelliv, 12), dtype = np.float32) # O3 climatology:
        moncycle = (zeit[:]*12%12).astype(int)
        # By this point assume that NaNs, MDIs have been screened out
        for nmois in np.arange(12):
            mon4mean = (moncycle == nmois).nonzero()[0]
            nmon4mean = np.size(mon4mean)
            if (nmon4mean > 0):    
                ozocliml[0,:,:,nmois] = np.mean(ozonncdf[0,:,:,mon4mean], axis=0, dtype = np.float32)
        zeitncdf = np.arange(12) * 30. + 15.
        timeunit = 'days since 0000-01-01 00:00:00'
        timeorigin = '01-JAN-0000:00:00:00'
        ozonncdf = ozocliml
        
    else:    
        if verbose:    
            print 'Preparing timeseries information ... '
        #
        iniyear = np.int(zeit[0]) # for example 1978
        ## SMO change
        inimont = 1 + (zeit[0]*12%12).astype(int) # for example 11
        #inimont=zeit[0]+1
        namarrmon = array(['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'])
        namemont = namarrmon[inimont - 1]
        timeunit = 'days since ' + '%4d'%iniyear + '-' + '%02d'%inimont + '-01 00:00:00'
        timeorigin = '01-' + namemont + '-' + '%4d'%iniyear + ':00:00:00'
        zeitncdf = np.arange(ntime) * 30. + 15.
    
    # -------------------------------------------------------------------------
    # Rescale ozone by a specified amount and allow smear to 3D for LAM masters
    # -------------------------------------------------------------------------
    
    if len(lamlong) == 0:    
        ozonncdf = ozonncdf * multiply
        newlongi = 360. # This means that it is a zonal mean.
    else:    
        ozondims = ozonncdf.shape
        ozolam = np.zeros((lamlong,) + ozondims[1:4])
        # Not 100% sure this is how IDL rebin expands,
        # but I believe it just copies!
        ozolam[:,:,:,:] = ozonncdf[0,:,:,:] * multiply
        # Calculate all the longitudes
        dx = 360. / lamlong
        if endgame:    
            firstx = 0.5 * dx
        else:    
            firstx = 0.0
        newlongi = firstx + np.arange(lamlong) * dx
        sudno = '3D' + sudno
    
    if verbose:    
        print 'Processing of data successfully completed.'
    
    # ------------------------------------
    # ------------------------------------
    # Prepare OUTPUT information and data.
    # ------------------------------------
    # ------------------------------------
    
    # Coordinate variables must keep the same name for the variable
    # dimension and the variable itself:
    namelong = 'longitude'
    namelati = 'latitude'
    nameheig = 'hybrid_ht'
    nameozon = 'O3'
    namezeit = 't'
    
    if clim:    
        stproc = 'M'
    else:    
        stproc = 'Y'
    styear = '%4d'%zeit[0]
    if (np.int(zeit[ntime - 1]) != np.int(zeit[0])):    
        styear = styear + '-' + '%4d'%zeit[ntime - 1]
    namencdf = outdir + 'Ozone_CMIP5_SPARCex_' + stproc + styear + '_' + sudno + nolev + colo3 + '.nc'
    if verbose:    
        print 'Prepare output to file ', namencdf
    
    # ---------------------------
    # The netcdf file is created:
    # ---------------------------
    
    zahldate = Dataset(namencdf, 'w', format='NETCDF3_CLASSIC')
    
    # ---------------------------------
    # Variable [0] namelong='longitude'
    # ---------------------------------
    
    # The ozone data are zonal averages:
    dimelong = zahldate.createDimension(namelong, np.size(newlongi))
    varilong = zahldate.createVariable(namelong, np.float32, (namelong,))
    varilong.units = 'degrees_east'
    varilong.point_spacing = 'even'
    # The above must be units in order to be correctly interpreted by xconv.
    
    # --------------------------------
    # Variable [1] namelati='latitude'
    # --------------------------------
    
    dimelati = zahldate.createDimension(namelati, np.size(newlatit))
    varilati = zahldate.createVariable(namelati, np.float32, (namelati,))
    varilati.units = 'degrees_north'
    varilati.point_spacing = 'even'
    
    # ---------------------------------
    # Variable [2] nameheig='hybrid_ht'
    # ---------------------------------
    
    dimehoeh = zahldate.createDimension(nameheig, np.size(heightum))
    variheig = zahldate.createVariable(nameheig, np.float32, (nameheig,))
    variheig.units = 'm'
    variheig.positive = 'up'
    # The above attribute is recommended for the vertical axis
    
    # -------------------------
    # Variable [3] namezeit='t'
    # -------------------------
    
    dimezeit = zahldate.createDimension(namezeit, np.size(zeitncdf))
    varizeit = zahldate.createVariable(namezeit, np.float32, (namezeit,))
    varizeit.units = timeunit
    varizeit.calendar = '360_day'
    varizeit.time_origin = timeorigin
    
    # --------------------------
    # Variable [4] nameozon='O3'
    # --------------------------
    
    # If an ancillary needs be created, data have to be in mass mixing ratios
    # The 1.0e-6 is to counteract multiplication by 1.0e6 above
    ppmv2mmr = (1.0e-6 * Mozo / Mair)
    if massmix:
        ozonncdf = ozonncdf * ppmv2mmr 
        ozounit = 'mass mixing ratio'
    else:   
        ozounit = 'ppmv'
    #
    variozon = zahldate.createVariable(nameozon, np.float32, (namezeit,nameheig,namelati,namelong))
    variozon.source = data_from
    variozon.setncattr('name', nameozon)
    variozon.title = 'Ozone ' + ozounit
    variozon.long_name = nameozon + ', SPARC Ozone'
    variozon.units = 'kg kg^-1'
    
    zahldate.source = 'AC&C (http://www.igac.noaa.gov/ACandC.php) / SPARC (http://www.atmosp.physics.utoronto.ca/SPARC) / NCAS (http://ncas-climate.nerc.ac.uk/chris)'
    
    zahldate.contact = 'Scott Osprey (s.osprey@physics.ox.ac.uk), Chris Bell (--) High-top extension and Jean Francois Lamarque (lamar@ucar.edu), Bill Randel(randel@ucar.edu); Veronika Eyring (veronika.eyring@dlr.de) for original SPARC compilation'
    
    zahldate.title = 'High-Top extension of AC&C/SPARC Ozone Database for CMIP5'
    
    # ----------------------------------------------------
    # Completed Define Mode, now move to Data mode Output.
    # ----------------------------------------------------
    varilong[:] = newlongi
    varilati[:] = newlatit
    variheig[:] = heightum
    varizeit[:] = zeitncdf
    # Get the ozone in the right shape
    variozon[:,:,:,:] = np.swapaxes(np.swapaxes(ozonncdf,1,2),0,3)
    
    zahldate.close()
    # ----------------------------
    # The netcdf file is complete:
    # ----------------------------
    if verbose:    
        print 'Your file has been written.'
    
    return
